app.factory('globalService', [function () {
    var global = {
        enums: {
            product: {
                "common": {
                    valueOf: function () {
                        return 0;
                    },
                    toString:function(){
                        return "common";
                    },
                    requestType:'get'
                },
                "photos": {
                    valueOf: function () {
                        return 1;
                    },
                    toString: function () {
                        return "photos";
                    },
                    requestType:'query'
                },
                "comments": {
                    valueOf: function () {
                        return 2;
                    },
                    toString: function () {
                        return "comments";
                    },
                    requestType:'query'
                }
            }
        }
    };
    return global;
}]);