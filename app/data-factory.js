angular.module('multiApp.dataLayer',['ngResource']);

angular.module('multiApp.dataLayer').factory('dataFactory', ['$http','$resource', function ($http,$resource) {
    var categoryResource= $resource('/api/categories/:name',{name: '@name'});
    var productResource = $resource('/api/product/:productId/:infoType',{productId: '@productId',infoType: '@infoType'});

    return {
        categoryResource: categoryResource,
        productResource:productResource,
    }
}]);