app.controller('productController', ['$scope', 'productService', '$stateParams', 'productInfo', function ($scope, productService, $stateParams, productInfo) {
    $scope.product = productService.selectedProduct;
    $scope.product.activeTab = productService.enums.product[$stateParams.infoType];
    $scope.product.activePage = $scope.product[$stateParams.infoType];
}])