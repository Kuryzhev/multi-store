app.factory('productService', ['categoryService', 'dataFactory', function (categoryService, dataFactory) {
    var base = Object.create(categoryService);

    base.getProductPromise = function (productId, infoType) {
        var existing = _.findWhere(base.categoryProducts, {id: Number(productId)});
        base.selectedProduct = existing;

        if (existing[infoType])
            return existing[infoType];

        var promise = dataFactory.productResource[base.enums.product[infoType].requestType]({
            productId: productId,
            infoType: infoType
        }).$promise;

        promise.then(function (respInfo) {
            base.selectedProduct[infoType] = respInfo;
        });
        return promise;
    };
    return base;
}]);