app.factory('categoryService', ['globalService', 'dataFactory', function (globalService, dataFactory) {
    var base = Object.create(globalService);

    function initialiseProduct(obj) {
        function product(obj) {
            _.each(Object.keys(obj), function (key) {
                this[key] = obj[key];
            }.bind(this));
        }
        product.prototype = angular.extend(obj.constructor.prototype, {
            addPrice: function(){
                this.price++;
            }
        });
        return new product(obj);
    }

    base.getCategoryProductsPromise = function (categoryName) {
        if (base.currentCategory == categoryName)
            return base.categoryProducts;

        var promise = dataFactory.categoryResource.query({name: categoryName}).$promise;
        promise.then(function (categoryProducts) {
            base.categoryProducts = [];
            _.each(categoryProducts, function (resProduct) {
                resProduct.id = Number(resProduct.id);
                resProduct.price = Number(resProduct.price);
                base.categoryProducts.push(initialiseProduct(resProduct))
            });
        });
        base.currentCategory = categoryName;
        return promise;
    };
    return base;
}]);