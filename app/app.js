var app = angular.module('multiApp', ['multiApp.dataLayer', 'ui.router']);

app.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function ($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider
        .state('main', {
            url: "/",
            absctract: true,
            templateUrl: 'app/global.html',
            controller: 'globalController',
            resolve: {
                categories: ['dataFactory', function (dataFactory) {
                    return dataFactory.categoryResource.query().$promise;
                }]
            }
        })
        .state('main.home', {
            url: "home",
            templateUrl: 'app/home/home.html',
            controller: 'homeController',
        })
        .state('main.category', {
            url: ":categoryName/",
            templateUrl: 'app/category/category.html',
            controller: 'categoryController',
            resolve: {
                categoryProducts: ['categoryService', '$stateParams','categories', function (categoryService, $stateParams,categories) {
                    return categoryService.getCategoryProductsPromise($stateParams.categoryName)
                }]
            }
        })
        .state('main.category.product', {
            url: ":productId?infoType/",
            views: {
                '@main': {
                    templateUrl: 'app/category/product/product.html',
                    controller: 'productController',
                }
            },
            resolve: {
                productInfo: ['productService', '$stateParams','categoryProducts', function (productService, $stateParams,categoryProducts) {
                    if (!productService.enums.product[$stateParams.infoType])
                        //or redirect to 404 page
                        $stateParams.infoType = productService.enums.product.common.toString();
                    return productService.getProductPromise($stateParams.productId,$stateParams.infoType)
                }]
            }
        });

    $urlRouterProvider.when("", "/home");
    $urlRouterProvider.when("/#/", "/home");
}]);