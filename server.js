var express  = require('express');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var fs = require("fs");
var app = express();
var dataUrl='/data/';   //data location

app.use(bodyParser.urlencoded({'extended':'true'}));
app.use(bodyParser.json());
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));
app.use(methodOverride());
app.use(express.static(__dirname));

app.get('/api/categories', function(req, res) {
    fs.readFile(__dirname.concat(dataUrl,"categories.json"), 'utf8', function (err, data) {
        res.json(JSON.parse(data));
    });
});

app.get('/api/categories/:name', function(req, res) {
    fs.readFile(__dirname.concat(dataUrl,req.params.name,".json"), 'utf8', function (err, data) {
        res.json(JSON.parse(data));
    });
});
app.get('/api/product/:productId/:infoType', function(req, res) {
    var result = {
        about: "product with id: " + req.params.productId + "is great one!",
        info: "some full info"
    };
    switch(req.params.infoType){
        case "common":
            result = {
                about: "product with id: " + req.params.productId + "is great one!",
                info: "some full info"
            };
            break;
        case "photos":
            result = ["photo1","photo2","photo3"];
            break;
        case "comments":
            result = ["comment1","comment2","comment3"];
            break;
    }
    res.json(result);
});

//app.post('/api/customers/:email', function(req, res) {
//    fs.writeFile(__dirname + dataUrl + req.params.email+".json", JSON.stringify(req.body), function(error) {
//        res.sendStatus( error ? 500 : 200);
//    });
//    if (req.body.email && fs.existsSync(__dirname + dataUrl + req.params.email+".json"))
//        fs.rename(__dirname + dataUrl + req.params.email+".json", __dirname + dataUrl + req.body.email+".json");
//});
//
//app.delete('/api/customers/:email', function(req, res) {
//    fs.unlink(__dirname + dataUrl + req.params.email+".json", function(error) {
//        res.sendStatus( error ? 500 : 200);
//    });
//});

app.get('/', function(req, res) {
    if (req.url === '/favicon.ico') {
        res.writeHead(200, {'Content-Type': 'image/x-icon'} );
        res.end();
        return;
    }
    res.sendFile('index.html', { root: __dirname });
});
app.listen(8086);
console.log("App listening on port 8080");
